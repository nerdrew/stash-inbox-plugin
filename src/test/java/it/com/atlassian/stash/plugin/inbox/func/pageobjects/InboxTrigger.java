package it.com.atlassian.stash.plugin.inbox.func.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class InboxTrigger {

    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "inbox-pull-requests")
    private PageElement inboxTrigger;

    @WaitUntil
    public void waitUntilCountLoaded() {
        Poller.waitUntilTrue(inboxTrigger.timed().hasAttribute("data-countLoaded", "true"));
    }

    public TimedCondition isCountVisible() {
        return inboxTrigger.find(By.className("aui-badge")).timed().hasClass("visible");
    }

    public String getCountText() {
        return inboxTrigger.getText();
    }

    public InboxDialog open() {
        inboxTrigger.click();
        return pageBinder.bind(InboxDialog.class);
    }

    public TimedCondition inboxIsEmpty() {
        return inboxTrigger.find(By.className("aui-iconfont-workbox-empty")).timed().isPresent();
    }
}
