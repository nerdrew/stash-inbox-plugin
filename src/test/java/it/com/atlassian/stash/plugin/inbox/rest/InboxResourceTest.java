package it.com.atlassian.stash.plugin.inbox.rest;

import com.atlassian.stash.pull.PullRequestRole;
import com.atlassian.stash.test.DefaultFuncTestData;
import com.atlassian.stash.test.PullRequestTestHelper;
import com.atlassian.stash.test.TestContext;
import com.atlassian.stash.user.Permission;
import com.jayway.restassured.RestAssured;
import org.junit.*;
import org.springframework.core.io.ClassPathResource;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;


public class InboxResourceTest {

    private static final String INBOX_PROJ = "PR_TEST";
    private static final String INBOX_REPO = "inbox_repo";
    private static final String GIT_PR_ZIP = "git/pull-requests.zip";
    private static final String SOURCE_BRANCH = "branch_that_has_same_file_modified_and_moved_src";
    private static final String TARGET_BRANCH = "branch_that_has_same_file_modified_and_moved_trgt";
    private static final String CREATOR = "pr_creator";
    private static final String REVIEWER = "pr_reviewer";
    private static final String PR_TITLE = "Some pull request";

    protected PullRequestTestHelper pullRequest;

    @ClassRule
    public static TestContext classTestContext = new TestContext();

    @Rule
    public TestContext testContext = new TestContext();

    @BeforeClass
    public static void setup() throws Exception {
        classTestContext
                .project(INBOX_PROJ)
                .repository(INBOX_PROJ, INBOX_REPO, new ClassPathResource(GIT_PR_ZIP))
                .user(CREATOR)
                .user(REVIEWER)
                .repositoryPermission(INBOX_PROJ, INBOX_REPO, CREATOR, Permission.REPO_WRITE)
                .repositoryPermission(INBOX_PROJ, INBOX_REPO, REVIEWER, Permission.REPO_READ);
    }

    @Before
    public void setupPullRequest() {
        pullRequest = testContext.pullRequest(new PullRequestTestHelper.Builder(CREATOR, CREATOR, PR_TITLE, "Description",
                INBOX_PROJ, INBOX_REPO, SOURCE_BRANCH, INBOX_PROJ, INBOX_REPO, TARGET_BRANCH).reviewers(REVIEWER));
    }

    @Test
    public void testInboxPullRequestCount() {
        // PR counted only if reviewer, not as creator
        RestAssured.expect()
                .statusCode(OK.getStatusCode())
                .body("count", equalTo(0))
                .log().ifError()
                .given()
                .auth().preemptive().basic(CREATOR, CREATOR)
                .when()
                .get(getInboxCountUrl());

        RestAssured.expect()
                .statusCode(OK.getStatusCode())
                .body("count", equalTo(1))
                .log().ifError()
                .given()
                .auth().preemptive().basic(REVIEWER, REVIEWER)
                .when()
                .get(getInboxCountUrl());

        pullRequest.approveBy(REVIEWER);

        RestAssured.expect()
                .statusCode(OK.getStatusCode())
                .body("count", equalTo(0))
                .log().ifError()
                .given()
                .auth().preemptive().basic(REVIEWER, REVIEWER)
                .when()
                .get(getInboxCountUrl());

    }

    @Test
    public void testInboxPullRequestsAsAuthor() {
        RestAssured.expect()
                .statusCode(OK.getStatusCode())
                .body("size", equalTo(1))
                .body("values[0].id", equalTo((int) pullRequest.getId()))
                .log().ifError()
                .given()
                .auth().preemptive().basic(CREATOR, CREATOR)
                .when()
                .get(getInboxUrlForRole(PullRequestRole.AUTHOR.name()));

        pullRequest.declineSafe();

        // Only open pull requests are included in inbox
        RestAssured.expect()
                .statusCode(OK.getStatusCode())
                .body("size", equalTo(0))
                .body("values", empty())
                .log().ifError()
                .given()
                .auth().preemptive().basic(REVIEWER, REVIEWER)
                .when()
                .get(getInboxUrlForRole(PullRequestRole.AUTHOR.name()));
    }

    @Test
    public void testInboxPullRequestsAsReviewer() {
        RestAssured.expect()
                .statusCode(OK.getStatusCode())
                .body("size", equalTo(1))
                .body("values[0].id", equalTo((int) pullRequest.getId()))
                .log().ifError()
                .given()
                .auth().preemptive().basic(REVIEWER, REVIEWER)
                .when()
                .get(getInboxUrlForRole(PullRequestRole.REVIEWER.name()));

        pullRequest.approveBy(REVIEWER);

        RestAssured.expect()
                .statusCode(OK.getStatusCode())
                .body("size", equalTo(0))
                .body("values", empty())
                .log().ifError()
                .given()
                .auth().preemptive().basic(REVIEWER, REVIEWER)
                .when()
                .get(getInboxUrlForRole(PullRequestRole.REVIEWER.name()));

    }

    @Test
    public void testInboxPullRequestsInvalidRole() {
        RestAssured.expect()
                .statusCode(BAD_REQUEST.getStatusCode())
                .given()
                .auth().preemptive().basic(REVIEWER, REVIEWER)
                .when()
                .get(getInboxUrlForRole("admin"));
    }

    private String getInboxUrl() {
        return DefaultFuncTestData.getRestURL("inbox", "latest") + "/pull-requests";
    }

    private String getInboxUrlForRole(String role) {
        return getInboxUrl() + "?role=" + role;
    }

    private String getInboxCountUrl() {
        return getInboxUrl() + "/count";
    }

}
