package it.com.atlassian.stash.plugin.inbox.func.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.components.ActivatedComponent;
import com.atlassian.pageobjects.components.aui.AuiInlineDialog;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.inject.Inject;
import org.openqa.selenium.By;

/**
 * Created by mszczepanski on 10/07/2014.
 */
public class FeatureDiscoveryDialog {
    private final String feature;

    @Inject
    PageElementFinder pageElementFinder;

    public FeatureDiscoveryDialog(String feature) {
        this.feature = feature;
    }

    private String getId() {
        return "inline-dialog-feature-discovery-" + feature;
    }

    public boolean exists() {
        PageElement dialog = pageElementFinder.find(By.id(getId()));
        return dialog != null && dialog.isPresent() && dialog.isVisible();
    }

    public void close() {
        PageElement closeButton = pageElementFinder.find(By.className("aui-button-feature-discovery-" + feature));
        closeButton.click();
        Poller.waitUntilFalse("dialog should go away", pageElementFinder.find(By.id(getId())).timed().isVisible());
    }
}
