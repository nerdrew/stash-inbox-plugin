package it.com.atlassian.stash.plugin.inbox.func.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.stash.element.PullRequestList;
import com.atlassian.webdriver.stash.element.TabItem;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;

public class InboxDialog {

    private static final String REVIEWING_TAB = "Reviewing";
    private static final String CREATED_TAB = "Created";

    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "inline-dialog-inbox-pull-requests-content")
    private PageElement inboxDialog;

    @WaitUntil
    public void waitTabIsLoaded() {
        Poller.waitUntilFalse(inboxDialog.find(By.cssSelector(".active-pane .spinner")).timed().isVisible());
    }

    public PullRequestList getActivePullRequestList() {
        return pageBinder.bind(PullRequestList.class, By.cssSelector("#inline-dialog-inbox-pull-requests-content .active-pane .pull-requests-table"), TimeoutType.DEFAULT);
    }

    public boolean isShowingReviewing() {
        return getActivePullRequestListName().equals(REVIEWING_TAB);
    }

    public boolean isShowingCreated() {
        return getActivePullRequestListName().equals(CREATED_TAB);
    }

    public void switchToReviewing() {
        getTab(REVIEWING_TAB).click();
        waitTabIsLoaded();
    }

    public void switchToCreated() {
        getTab(CREATED_TAB).click();
        waitTabIsLoaded();
    }

    public boolean isActivePullRequestListEmpty() {
        return inboxDialog.find(By.cssSelector(".active-pane .aui-iconfont-workbox-empty")).isPresent();
    }

    public boolean isOpen() {
        return inboxDialog.isVisible();
    }

    private String getActivePullRequestListName() {
        return inboxDialog.find(By.className("active-tab")).getText();
    }

    private Collection<TabItem> getTabItems() {
        if (isOpen()) {
            return inboxDialog.findAll(By.cssSelector(".aui-tabs > .tabs-menu > li"), TabItem.class);
        } else {
            return Collections.emptyList();
        }
    }

    private TabItem getTab(String text) {
        for (TabItem tab : getTabItems()) {
            if (tab.getText().trim().equalsIgnoreCase(text.trim())) {
                return tab;
            }
        }
        throw new NoSuchElementException(text + " tab not found");
    }

}
