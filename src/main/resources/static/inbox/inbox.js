define('plugin/inbox/inbox', [
    'aui',
    'jquery',
    'underscore',
    'stash/api/util/events',
    'stash/api/util/navbuilder',
    'stash/api/util/state',
    'stash/api/util/server',
    'exports'
], function (
    AJS,
    $,
    _,
    events,
    navBuilder,
    pageState,
    server,
    exports
) {
    var inlineDialog,
        $inboxTrigger;

    function getInboxResourceUrlBuilder(role) {
        return navBuilder.newBuilder().addPathComponents('rest', 'inbox', 'latest', 'pull-requests').withParams({role: role});
    }

    function getInboxCountResourceUrl() {
        return navBuilder.newBuilder().addPathComponents('rest', 'inbox', 'latest', 'pull-requests', 'count').build();
    }

    var hideOnEscapeKeyUp = function(e) {
        if(e.keyCode === $.ui.keyCode.ESCAPE) {
            inlineDialog.hide();
            e.preventDefault();
        }
    };

	var hideOnDialogShown = function () {
		inlineDialog.hide();
	};

    var handleInboxError = function($content, response) {
        var defaultError = {
            title: AJS.I18n.getText('stash.plugin.inbox.error.title'),
            message: AJS.I18n.getText('stash.plugin.inbox.error.unknown')
        };

        var responseError = {};
        if (response) {
            responseError = response.errors ?
                response.errors[0] :
                response;
        }
        var error = $.extend({}, defaultError, responseError);

        $content.html($(stash.widget.focusMessage.error({
            title: error.title,
            text: error.message,
            extraClasses: 'communication-error'
        })));
        return false;
    };

    function initialiseDialog($container) {
        initialiseDialogWithRoles($container, [
            {id: 'reviewer', title: AJS.I18n.getText('stash.plugin.inbox.tabs.reviewer')},
            {id: 'author', title: AJS.I18n.getText('stash.plugin.inbox.tabs.author')},
            {id: 'participant', title: AJS.I18n.getText('stash.plugin.inbox.tabs.participant')}
        ]);
    }

    function initialiseDialogWithRoles($container, roles) {
        var AvatarList = require('widget/avatar-list');
        var PullRequestsTable = require('feature/pull-request/pull-request-table');
        var $dialogContents = $(stash.plugin.inbox.dialogContents({roles: roles}));

        $container.append($dialogContents);

        var handleError = function(xhr, textStatus, errorThrown, resp) {
            return handleInboxError.call(this, $container, resp);
        };

        function initPullRequestTable(role, showOnLoad) {
            var pullRequestTable = new PullRequestsTable('open', null, _.partial(getInboxResourceUrlBuilder, role), {
                'scope': 'global',
                'target': '#inbox-pull-request-table-' + role,
                'scrollPaneSelector': '.inbox-table-wrapper',
                'bufferPixels': 50,
                'pageSize': 10,
                'spinnerSize': 'medium',
                'noneFoundMessageHtml': stash.plugin.inbox.emptyInboxMessage(),
                'dataLoadedEvent': 'stash.plugin.inbox.dataLoaded.' + role,
                'statusCode': {
                    0: handleError,
                    401: handleError,
                    500: handleError,
                    502: handleError
                }
            });
            pullRequestTable.handleErrors = $.noop; // we handle the errors ourselves
            pullRequestTable.init();

            if (showOnLoad) {
                AJS.tabs.change($container.find('[href=#inbox-pull-request-' + role + ']'));
            }
        }

        _.each(roles, function(role, i) {
            initPullRequestTable(role.id, i === 0);
        });

        // Unfortunately AJS.tabs.setup() is called before we get a chance to create the HTML, need to do this manually
        $container.find('.tabs-menu').delegate("a", "click", function (e) {
            AJS.tabs.change($(this), e);
            e && e.preventDefault();
        });
        AvatarList.init();
    }

    var onShowDialog = function ($content, trigger, showPopup) {
        showPopup();
        $(document).on('keyup', hideOnEscapeKeyUp);

		/*
		 * This has been added for interaction with the other dialogs using dialog2,
		 * and so the inline dialog for the inbox doesn't know about them which causes
		 * layering issues when they're opened from the PR list.
		 *
		 * This might be able to be removed in future if there's an InlineDialog2
		 * and the Inbox is updated to use it.
		 */
		AJS.dialog2.on('show', hideOnDialogShown);

        loadDialogResources($content, _.partial(initialiseDialog, $content));
    };

    var loadDialogResources = _.once(function($content, callback) {
        var $spinner = $('<div class="loading-resource-spinner"></div>');
        $content.empty().append($spinner);
        $spinner.show().spin('medium');

        WRM.require('wrc!stash.pullRequest.inbox').always(function() {
            $spinner.spinStop().remove();
        }).done(function() {
            callback();
        });
    });

    var onHideDialog = function () {
        $(document).off('keyup', hideOnEscapeKeyUp);
		AJS.dialog2.off('show', hideOnDialogShown);

        if ($(document.activeElement).closest('#inbox-pull-requests-content').length) {
            // if the focus is inside the dialog, you get stuck when it closes.
            document.activeElement.blur();
        }
    };

    var fetchInboxCount = function() {
        server.rest({
            url: getInboxCountResourceUrl(),
            type: 'GET',
            statusCode: {
                '*': false
            }
        }).done(function (data) {
            if (data.count > 0) {
                var $badge = $(aui.badges.badge({
                    'text': data.count
                }));
                $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: false}))
                    .append($badge);
                setTimeout(function() {
                    // Needed for the transition to trigger
                    $badge.addClass('visible');
                }, 0);
            } else {
                // The badge fadeOut transition happens with a CSS3 transition, which we can't hook into.
                // Use a setTimeout instead, unfortunately.
                var cssTransitionDuration = 500;
                $inboxTrigger.find(".aui-badge").removeClass('visible');
                setTimeout(function () {
                    $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: true}));
                }, cssTransitionDuration);
            }
            $inboxTrigger.attr('data-countLoaded', true);
        });
    };

    exports.onReady = function () {
        $inboxTrigger = $("#inbox-pull-requests");
        if ($inboxTrigger.length && pageState.getCurrentUser()) {
            $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: true}));
            inlineDialog = AJS.InlineDialog($inboxTrigger, 'inbox-pull-requests-content', onShowDialog, {
                width: 870,
                hideCallback: onHideDialog
            });

            fetchInboxCount();

            var _approvalHandler = function(data) {
                if (data.user.name === pageState.getCurrentUser().name) {
                    fetchInboxCount();
                }
            };

            events.on('stash.widget.approve-button.added', _approvalHandler);
            events.on('stash.widget.approve-button.removed', _approvalHandler);
        }
    };
});

AJS.$(document).ready(function () {
    require('plugin/inbox/inbox').onReady();
});
